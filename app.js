/** require package **/
/******************************************/
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken')
const io = require('socket.io')(server)
require('dotenv/config')
app.use(bodyParser.json());
/******************************************/

/* import routes */
/******************************************/
const usersRoute = require('./routes/users')
const authRoute = require('./routes/auth')
const linkRoute = require('./routes/links')
const reportRoute = require('./routes/reports')
const gozareshRoute = require('./routes/gozareshes')
const jokeRoute = require('./routes/jokes');
const { decode } = require('punycode');
app.use('/api/users', usersRoute)
app.use('/api', authRoute);
app.use('/api/links', linkRoute);
app.use('/api/reports', reportRoute);
app.use('/api/gozaresh', gozareshRoute);
app.use('/api/jokes', jokeRoute);
/******************************************/


/** Chat Socket **/
/******************************************/



io.on('connection', (socket) => {
    console.log('connected');

    socket.on('private', ({ userObj, message, id, sticker }) => {

        io.emit(id, ({ userObj, message, sticker }));

    })

    /* room1 */
    socket.on('room1', ({ message, userObj, avatar, sticker, reply, status }) => {

        io.emit('message1', { message, userObj, avatar, sticker, reply, status });

    });

    /*room2*/


    socket.on('room2', ({ message, userObj, avatar, sticker, reply, status }) => {

        io.emit('message2', { message, userObj, avatar, sticker, reply, status });

    });

    /* room3 */

    socket.on('room3', ({ message, userObj, avatar, sticker, reply, status }) => {

        io.emit('message3', { message, userObj, avatar, sticker, reply, status });

    });

    socket.on('report', ({ id }) => {
        
        console.log('report'+id)
    })

})




/******************************************/

/** db connection **/
/******************************************/

mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true }, () => console.log('db connected'))
/******************************************/

// server code
/******************************************/

server.listen(3200, () => {
    console.log('listening port 3000 ...')
})













// socket.on('join', ({name, room, token}, callback) => {
//     console.log(name, room, token);
//     if (token) {
//         const verifyCheck = verify(token);
//         if (verifyCheck) {
//             console.log(connectedUsers);
//             const {error, user} =  addUser({id: socket.id, name, room})
//             if (error) {
//                 return callback({status:400});
//             }

//             socket.emit('message', {
//                 user: 'admin',
//                 text: `${user.name}, welcome to the room ${user.room}.`,
//                 time: Date.now(),
//                 userObj:get_user(token)
//             });
//             socket.broadcast.to(user.room).emit('message', {
//                 user: 'admin',
//                 text: `${user.name} has joined.`,
//                 time: Date.now()
//             });

//             socket.join(user.room);
//             io.to(user.room).emit('roomData', {room: user.room, users: getUserInRoom(user.room)})
//             // callback({status: 200});

//         }
//     }
//     // callback({status: 401})
// })

// socket.on('sendMessage', ({message, token,status,avatar}, callback) => {
//     if (token) {
//         const verifyCheck = verify(token);
//         if (verifyCheck) {

//             const user = getUser(socket.id);
//             io.to(user.room).emit('message', {user: user.name, text: message, time: Date.now(),userObj:get_user(token),status,avatar});
//             io.to(user.room).emit('roomData', {room: user.room, users: getUserInRoom(user.room)});
//             // callback({status: 200});
//         }
//     } //else callback({status: 401});
// })
















// socket.on('private', ({ receiver, sender, message, status, avatar }) => {
//     console.log(receiver)
//     receiver = getUserPrivate(receiver)
//     if (receiver) {
//         console.log(receiver)
//         const recieverSocket = receiver.id;
//         socket.to(recieverSocket).emit('private-message', {
//             user: sender,
//             text: message,
//             time: Date.now(),
//              status, avatar

//         })
//         socket.emit('private-message', { user: sender, text: message, time: Date.now(), status, avatar })

//         console.log('message send private')
//     }


// })

// socket.on('connect-user', ({ name }) => {

//             addUserPrivate({ id: socket.id, name: name });
//             // console.log(usersGet());


// })
