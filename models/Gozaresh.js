const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');


const GozareshtSchema = mongoose.Schema({
    reporter: {
        type: String,
        required: true,

    },
    reporter_id: {
        type: String,
        required: true,

    },
    reporter_nicId: {
        type: String,
        required: true,
    },
    reportedName: {
        type: String,
        required: true,

    },
    reported_id: {
        type: String,
        required: true,

    },
    reported_nicId: {
        type: String,
        required: true,

    },
    description: {
        type: String,
        required: true,
    },
   

    date: {
        type: String,
        required : true
    },
});


module.exports = mongoose.model('Gozaresh', GozareshtSchema)