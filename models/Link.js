const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');


const LinkSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,


    },
    title: {
        type: String,
        required: true,


    },
    url: {
        type: String,
        required: true
    },
    is_visible: {
        type: Boolean,
        required: true,
        default :0
    }
});


module.exports = mongoose.model('Link', LinkSchema)