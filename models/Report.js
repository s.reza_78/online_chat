const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');


const ReportSchema = mongoose.Schema({
    reporter: {
        type: String,
        required: true,

    },
    reportedName: {
        type: String,
        
    },
    description: {
        type: String,
        required: true,
    },
    reported_nicId: {
        type: String,
        required: true,

    },
    reporter_nicId: {
        type: String,
        required: true,
    },
    reported_level:{
        type: String,
        required: true,
    },
    reporter_level:{
        type: String,
        required: true,
    },

    date: {
        type: String,
        required : true
    },
});


module.exports = mongoose.model('Report', ReportSchema)
