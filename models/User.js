const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');


const UserSchema = mongoose.Schema({
    name: {
        type: String,
        default: 'user'


    },
    nic_id: {
        type: Number,
        unique: true

    },
    type: {
        type: String,
        default: 'کاربر',

    },
    report: {
        type: Boolean,
        default: 0
    },
    p_color: {
        type: Number,
        default: 0
    },
    g_color: {
        type: Number,
        default: 0
    },
    level: {
        type: Number,
        default: 0

    },
    hidden_report: {
        type: Boolean,
        default: 0

    },
    anti_report: {
        type: Boolean,
        default: 0

    },
    android_id: {
        type: String,
        report: true
    }
});
UserSchema.methods.toJSON = function () {
    var obj = this.toObject()
    delete obj.password
    return obj
}


function number_generrate() {



}

module.exports = mongoose.model('Users', UserSchema)



// module.exports = mongoose.model('Users', UserSchema);