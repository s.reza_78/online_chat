const express = require('express');
const jwt = require('jsonwebtoken')
const router = express.Router();
const User = require('../models/User')


router.post('/login',async (req , res)=>{

    try {
        const user = await User.findOne({username: new RegExp('^'+req.body.username+'$', "i")});

        if (user.password === req.body.password) {
            const token = jwt.sign({user: user}, 'secretkey')
            res.status(200).json({status: {'code': 200, 'message': 'Success'}, 'token': token, 'user': user})
        }
        else{
            res.status(401).json({status:{'code' : 401 ,'message': 'Unauthorized'}})

        }
    }
    catch (err){
    res.status(401).json({status:{'code' : 401 ,'message': 'Unauthorized'}})
    }

})


module.exports = router;