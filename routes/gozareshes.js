const express = require('express');
const router = express.Router();
const Gozaresh = require('../models/Gozaresh')
const User = require('../models/User')
const jwt = require('jsonwebtoken')
/** make report **/
router.post('/', async (req, res) => {
    try {
        const user = await User.findById(req.body.reportedId); /* reported*/
        const reporter = await User.findById(req.body.reporterId);
            const gozaresh = new Gozaresh({
                reporter: reporter.name,
                reportedName: user.name,
                reported_level: user.level,
                reporter_level: reporter.level,
                reported_nicId:user.nic_id,
                reporter_nicId:reporter.nic_id,
                reported_id:user.id,
                reporter_id:reporter.id,
                description: req.body.description,
                date: req.body.date,
            })
            const reportUser = await gozaresh.save();
            res.status(201).json({'status': {'code': 201, 'message': 'Success'}, reportUser})

        
    
    } catch (err) {
        res.status(400).json({'status': {'code': 400, 'message': 'Failed'}})
    }
});

/******************************************************/
/* get all reports */
router.get('/', async (req, res) => {
    try {
        const reports = await Gozaresh.find();
        // console.log(reports);
        res.status(200).json({'status': {'code': 200, 'message': 'Got all Users.'}, 'reports': reports});
    } catch (err) {
        res.status(400).json({'status': {'code': 400, 'message': 'Failed'}})

    }
});

/* get one report */
router.post('/find', async (req, res) => {
    try {
        const report = await Gozaresh.findOne({reported: req.body.reported});

        res.status(200).json({'status': {'code': 200, 'message': 'Report found . '}, 'reports': report});
    } catch (err) {
        res.status(400).json({'status': {'code': 400, 'message': 'Failed'}})

    }
});
/******************************************************/
/* delete one report */
router.delete('/:reportId', async (req, res) => {

    try {
        const removeReport = await Gozaresh.remove({_id: req.params.reportId});
        res.status(200).json({'status': {'code': 200, 'message': 'User Deleted'}, removeReport});
    } catch (err) {
        res.status(404).json({'status': {'code': 404, 'message': 'Failed'}})

    }
});




module.exports = router;