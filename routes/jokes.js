const express = require('express');
const router = express.Router();
const Joke = require('../models/Joke')

/** make jokes **/

router.post('/', async (req, res) => {
    try {
            const joke = new Joke({
                title: req.body.title,
                text: req.body.text,
                is_visible: req.body.is_visible,

            })
            const reportUser = await report.save();
            res.status(201).json({'status': {'code': 201, 'message': 'Success'}, joke})

    } catch (err) {
        res.status(400).json({'status': {'code': 400, 'message': 'Failed'}})
    }
});

/******************************************************/
/* get all reports */
router.get('/', async (req, res) => {
    try {
        const jokes = await Joke.find();
        console.log(jokes);
        res.status(200).json({'status': {'code': 200, 'message': 'Success'}, 'reports': jokes});
    } catch (err) {
        res.status(400).json({'status': {'code': 400, 'message': 'Failed'}})

    }
});

/* get one report */
router.post('/find', async (req, res) => {
    try {
        const joke = await Joke.findOne({reported: req.body.reported});

        res.status(200).json({'status': {'code': 200, 'message': 'Success '}, 'reports': joke});
    } catch (err) {
        res.status(400).json({'status': {'code': 400, 'message': 'Failed'}})

    }
});
/******************************************************/
/* delete one report */
router.delete('/:jokeId', async (req, res) => {

    try {
        const removeJoke = await Joke.remove({_id: req.params.jokeId});
        res.status(200).json({'status': {'code': 200, 'message': 'Deleted'}, removeJoke});
    } catch (err) {
        res.status(404).json({'status': {'code': 404, 'message': 'Failed'}})

    }
});

/* update report */
router.patch('/:jokeId', async (req, res) => {
    try {

        const updateJoke = await Joke.update(
            {_id: req.params.jokeId},
            {
                $set:
                    {
                        title: req.body.title,
                        text: req.body.text,
                        is_visible: req.body.is_visible,
                    }

            });

        res.status(200).json({'status': {'code': 200, 'message': 'Updated.', updateJoke}});


    } catch (err) {
        res.status(400).json({'status': {'code': 400, 'message': 'Failed'}})

    }
});

module.exports = router;