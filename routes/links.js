const express = require('express');
const router = express.Router();
const Link = require('../models/Link')
const User = require('../models/User')
router.post('/', async (req, res) => {


    const link = new Link({
        name: req.body.name,
        title: req.body.title,
        url: req.body.url,
        is_visible: req.body.is_visible,
    });
    try {
        const postLink = await link.save();
        res.status(201).json({'status': {'code': 201, 'message': 'Success'}, postLink})
    } catch (err) {
        res.status(400).json({'status': {'code': 400, 'message': 'Failed'}})
    }
});
/* first page get */
router.get('/', async (req, res) => {
    try {        
        const link = await Link.findOne({is_visible:1}).sort({_id: -1});
        const users = await User.find( { level: { $gte: 1} } ).sort({level:'desc'})
        console.log(users)
        res.status(200).json({'status': {'code': 200, 'message': 'Success'}, link :link , users:users})
    } catch (err) {
        res.status(400).json({'status': {'code': 400, 'message': 'Failed'}})
    }
});




module.exports = router;