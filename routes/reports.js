const express = require('express');
const router = express.Router();
const Report = require('../models/Report')
const User = require('../models/User')
const jwt = require('jsonwebtoken')
/** make report **/
router.post('/', async (req, res) => {
    try {
        const user = await User.findById(req.body.reportedId); /* reported*/
        const reporter = await User.findById(req.body.reporterId);
    
            const updateUser = await User.update(
                {_id: user.id},
                {
                    $set:
                        {
                            report: 1,
                        }

                });

            const report = new Report({
                reporter: reporter.name,
                reportedName: user.name,
                reported_nicId:user.nic_id,
                reporter_nicId:reporter.nic_id,
                reported_level: user.level,
                reporter_level: reporter.level,
                description: req.body.description,
                date: req.body.date,
            })
            const reportUser = await report.save();
            res.status(201).json({'status': {'code': 201, 'message': 'Success'}, reportUser})

        
    
    } catch (err) {
        res.status(400).json({'status': {'code': 400, 'message': 'Failed'}})
    }
});

/******************************************************/
/* get all reports */
router.get('/', async (req, res) => {
    try {
        const reports = await Report.find();
        console.log(reports);
        res.status(200).json({'status': {'code': 200, 'message': 'Got all Users.'}, 'reports': reports});
    } catch (err) {
        res.status(400).json({'status': {'code': 400, 'message': 'Failed'}})

    }
});

/* get one report */
router.post('/find', async (req, res) => {
    try {
        const report = await Report.findOne({reported: req.body.reported});

        res.status(200).json({'status': {'code': 200, 'message': 'Report found . '}, 'reports': report});
    } catch (err) {
        res.status(400).json({'status': {'code': 400, 'message': 'Failed'}})

    }
});
/******************************************************/
/* delete one report */
router.delete('/:reportId', async (req, res) => {

    try {
        const report = await Report.findById(req.params.reportId);
        const nic_id = report.reported_nicId;
        const user = await User.findOne({nic_id :nic_id});
        const updateUser = await User.update(
            {_id: user.id},
            {
                $set:
                    {
                        report: false
                    }

            });
        const removeReport = await Report.remove({_id: req.params.reportId});
        res.status(200).json({'status': {'code': 200, 'message': 'Seccess'}, removeReport});
    } catch (err) {
        res.status(404).json({'status': {'code': 404, 'message': 'Failed'}})

    }
});

/* update report */
router.patch('/:reportId', async (req, res) => {
    try {

        const updateReport = await Report.update(
            {_id: req.params.reportId},
            {$set: req.body});

        res.status(200).json({'status': {'code': 200, 'message': 'User Updated.', updateReport}});


    } catch (err) {
        res.status(400).json({'status': {'code': 400, 'message': 'Failed'}})

    }
});

router.get('/user-report', async (req, res) => {
    
    try {
        const users = await User.find({report:true});
        console.log(users);
        res.status(200).json({'status': {'code': 200, 'message': 'Got all Users.'}, 'users': users});
    } catch (err) {
        res.status(400).json({'status': {'code': 400, 'message': 'Failed'}})

    }
});
module.exports = router;
