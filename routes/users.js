const express = require('express');
const router = express.Router();
const User = require('../models/User')
const jwt = require('jsonwebtoken')
/** make user **/
router.post('/', async (req, res) => {
    // if (req.body.android_id) {
        const postUser = await User.findOne({ android_id: req.body.android_id });
    // }
    if (!postUser) {
       const user = new User({
            type: req.body.type,
            name: req.body.name,
            nic_id: Math.floor(100000 + Math.random() * 900000),
            p_color: req.body.p_color,
            g_color: req.body.g_color,
            level: req.body.level,
            anti_report: req.body.anti_report,
            report: req.body.report,
            hidden_report: req.body.hidden_report,
            android_id : req.body.android_id
        })
        try {
            const postUser = await user.save();
            res.status(201).json({ 'status': { 'code': 201, 'message': 'Success' }, postUser })
        } catch (err) {
            res.status(400).json({ 'status': { 'code': 400, 'message': 'Failed' } })
        }
    }
    else {
        try {
            res.status(201).json({ 'status': { 'code': 201, 'message': 'Success' }, postUser })
        } catch (err) {
            res.status(400).json({ 'status': { 'code': 400, 'message': 'Failed' } })
        }
    }

});

/******************************************************/
/* get all users */
router.get('/', async (req, res) => {
    try {
        const users = await User.find();
        console.log(users);
        res.status(200).json({ 'status': { 'code': 200, 'message': 'Got all Users.' }, 'users': users });
    } catch (err) {
        res.status(400).json({ 'status': { 'code': 400, 'message': 'Failed' } })

    }
});

/* get one user */
router.post('/find', async (req, res) => {
    try {

        if (req.body.nic_id.length <= 6) {
            const user = await User.findOne({ nic_id: req.body.nic_id });
            res.status(200).json({ 'status': { 'code': 200, 'message': 'User found . ' }, 'users': user });

        }
        else {
            const user = await User.findById({ _id: req.body.nic_id });
            res.status(200).json({ 'status': { 'code': 200, 'message': 'User found . ' }, 'users': user });
        }

    } catch (err) {
        res.status(400).json({ 'status': { 'code': 400, 'message': 'Failed' } })

    }
});
/******************************************************/
/* delete one user */
router.delete('/:userId', async (req, res) => {

    try {
        const removeUser = await User.remove({ _id: req.params.userId });
        res.status(200).json({ 'status': { 'code': 200, 'message': 'User Deleted' }, removeUser });
    } catch (err) {
        res.status(404).json({ 'status': { 'code': 404, 'message': 'Failed' } })

    }
});

router.patch('/:userId', async (req, res) => {
    try {

        const updateUser = await User.update(
            { _id: req.params.userId },
            { $set: req.body });
        res.status(200).json({ 'status': { 'code': 200, 'message': 'User Updated.', updateUser } });






    } catch (err) {
        res.status(400).json({ 'status': { 'code': 400, 'message': 'Failed' } })

    }
});
router.patch('/change-password/:userId', async (req, res) => {
    try {
        const user = await User.findById(req.params.userId);

        if (user.password === req.body.password) {
            const updateUser = await User.updateOne(
                user,
                {
                    $set:
                    {
                        password: req.body.newPass,
                    }

                });
            res.status(200).json({ 'status': { 'code': 200, 'message': 'User Updated.', updateUser } });
        } else {
            res.status(400).json({ 'status': { 'code': 400, 'message': 'Failed' } })
        }

    } catch (err) {
        res.status(400).json({ 'status': { 'code': 400, 'message': 'Failed' } })

    }
});




module.exports = router;